# Software Studio 2020 Spring Midterm Project
## Notice


## Topic
* Project Name : Midterm-Project-107062362
* Key functions (add/delete)
    1. chatting with private room
    2. sign in /out
    
* Other functions (add/delete)
    1. chrome notification
    2. css animation

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|NY|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm-project-107062362.web.app

# Components Description : 
1. signin/out : 
Referencing previous labs, we need to create certain firebase.auth() event to make it into practice. then/catch format would be very helpful when it comes to handling error.

![](https://i.imgur.com/2WIwckU.png)

2. Create room :
The project is designed for a 1:1 conversation application. In order to create a chat, you may need to type-in your target email. Notice you are not allowed to type in your own email.
![](https://i.imgur.com/YoxAk4a.png)

Also, the input will be valiadted as an email or not.
![](https://i.imgur.com/o63kQ74.png)

3. Send first chat :
For sending your first chat, we will need to see whether your current email matches your email you loggedd in. The same goes for the recevier for him/her to check the message when he/she login. We can uses a loop and if structure to achieve that.
![](https://i.imgur.com/3Mc6rpA.png)

4. Later chats:
For the record of the futher chatting, now we repeat the same idea but this time we need to check whether the sender and receiver are matched/grouped together in if-statement.
![](https://i.imgur.com/EPj0RrN.png)

5. Your message
When you click send, data including your email, receiver's email, your message and time will be pushed to firebase to achieve "write".
![](https://i.imgur.com/Z6rMxUy.png)

6. Current
Both 3, 4 are for showing chat history. The current message sending the handled by the function below. We add your current info to our database.
![](https://i.imgur.com/QwmD9oV.png)


# Other Functions Description : 
1. Third-Party signin : 
Again with previous labs, we can use a Google API GoogleAuthProvider to make it into practice.
![](https://i.imgur.com/2HY0yxq.png)

2. Chrome Notification : 
Check if the browser supports notifications, then proceed to check check whether notification permissions have already been granted. If so, notification will be sent.
![](https://i.imgur.com/yAxGiua.png)


## Security Report (Optional)
