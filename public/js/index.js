function init() {
    var user_email = '';
    var inbox_email = '';

    var menu = document.getElementById('dynamic-menu');


    firebase.auth().onAuthStateChanged(function(user) {
        
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function() {
                firebase.auth().signOut()
                    .then(function() {
                        alert('Sign Out!')
                        window.location = "signin.html";
                    })
                    .catch(function(error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
    var chat_history = firebase.database().ref('chat_history');

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function writeUserData(message) {
        chat_history.push({
            user_id: user_id,
            msg: message
        });
    }

    var enter_btn = document.getElementById('enter_btn');
    var post_btn = document.getElementById('post_btn');
    var post_txt = document.getElementById('comment');
    var flag = false;

    var inboxEmail = document.getElementById('inboxEmail');


    enter_btn.addEventListener('click', function () {
        if(validateEmail(inboxEmail.value)==false) {
            alert('Not allowed');
            inboxEmail.value = "Email entered failed";
        }
        else if (flag==false && user_email!=inboxEmail.value) {
                inbox_email = inboxEmail.value;
                inboxEmail.value = "Email entered successfully";
                firstTime();
        }
        else if (flag==true && user_email!=inboxEmail.value) {
                inbox_email = inboxEmail.value;
                inboxEmail.value = "Email entered successfully";
                firstPlusTimes();
        }
        else if(user_email==inboxEmail.value) {
            alert('Not allowed');
            inboxEmail.value = "Email entered failed";
        }
        else if(validateEmail(inboxEmail.value)==false) {
            alert('Not allowed');
            inboxEmail.value = "Email entered failed";
        }
    });

    var msg = document.getElementById('msg');

    function firstTime() {
        
        chat_history.once('value', function (snapshot) {
            msg.innerHTML='';
            var data = snapshot.val();
            for (var i in data) {
                if (user_email == data[i].sender) {
                    var text ='';
                    text+= '<span style="text-align:right; font-size:10px;">' + data[i].sender + ':' + '</span>';
                    text+= '<span style="text-align:right; background-color:lightblue; border-radius:5px;">' + data[i].message + '<br>';
                    text+= '<span style="float:right; background-color:lightblue; font-size:10px; border-radius:5px;">' + data[i].time + '</span>' + '</p>';

                    msg.innerHTML = msg.innerHTML + text;
                }
                else if(inbox_email == data[i].sender) {
                    var text ='';
                    text+= '<span style="text-align:left; font-size:10px;">' + data[i].sender + ':' + '</span>';
                    text+= '<span style="text-align:left; background-color:lightblue; border-radius:5px;">' + data[i].message + '<br>';
                    text+= '<span style="float:left; background-color:lightblue; font-size:10px; border-radius:5px;">' + data[i].time + '</span>' + '</p>';

                    msg.innerHTML = msg.innerHTML + text;
                }
                
                else continue;
            }
            msg.scrollTop = msg.scrollHeight;
        });
    }

    function firstPlusTimes() {
        chat_history.once('value', function (snapshot) {
            var data = snapshot.val();
            for (var i in data) {
                if(((user_email == data[i].sender) && (inbox_email == data[i].receiver)) || ((inbox_email == data[i].sender) && (user_email == data[i].receiver))) {
                    if (user_email == data[i].sender) {
                        var text ='';
                        text+= '<span style="text-align:right; font-size:10px;">' + data[i].sender + ':' + '</span>';
                        text+= '<span style="text-align:right; background-color:lightblue; border-radius:5px;">' + data[i].message + '<br>';
                        text+= '<span style="float:right; background-color:lightblue; font-size:10px; border-radius:5px;">' + data[i].time + '</span>' + '</p>';
    
                        msg.innerHTML = msg.innerHTML + text;
                    }
                    else if(inbox_email == data[i].sender) {
                        var text ='';
                        text+= '<span style="text-align:left; font-size:10px;">' + data[i].sender + ':' + '</span>';
                        text+= '<span style="text-align:left; background-color:lightblue; border-radius:5px;">' + data[i].message + '<br>';
                        text+= '<span style="float:left; background-color:lightblue; font-size:10px; border-radius:5px;">' + data[i].time + '</span>' + '</p>';
    
                        msg.innerHTML = msg.innerHTML + text;
                    }
                    else continue;
                }
                msg.scrollTop = msg.scrollHeight;
            }
        });
    }


    post_btn.addEventListener('click', function () {
        var date = new Date();
        var mo = date.getMonth()+1;
        var d = date.getDate();
        var h = date.getHours();
        var m = date.getMinutes();
        var t = d + '/' + mo + ' ' + h + ':' + m;
        if (inbox_email != "" && flag == false) {
            flag=true;
            chat_history.push({
                sender: user_email,
                receiver: inbox_email,
                message: post_txt.value,
                time: t
            });
            post_txt.value = '';
        }
        else if(flag==true) {
            chat_history.push({
                sender: user_email,
                receiver: inbox_email,
                message: post_txt.value,
                time: t
            });
            post_txt.value = '';
        }
        else {
            chat_history.push({
                sender: user_email,
                receiver: inbox_email,
                message: post_txt.value,
                time: t
            });
        }
    });

    chat_history.on('child_added', snapshot => {
        var data = snapshot.val();

        if(inbox_email != "") {
            var message = data.message;
            if (user_email == data.sender) {
                var text ='';
                text+= '<span style="text-align:right; font-size:10px;">' + data.sender + ':' + '</span>';
                text+= '<span style="text-align:right; background-color:lightblue; border-radius:5px;">' + data.message + '<br>';
                text+= '<span style="float:right; background-color:lightblue; font-size:10px; border-radius:5px;">' + data.time + '</span>' + '</p>';

                msg.innerHTML = msg.innerHTML + text;
            }
            else if (inbox_email == data.sender){
                var text ='';
                text+= '<span style="text-align:left; font-size:10px;">' + data.sender + ':' + '</span>';
                text+= '<span style="text-align:left; background-color:lightblue; border-radius:5px;">' + data.message + '<br>';
                text+= '<span style="float:left; background-color:lightblue; font-size:10px; border-radius:5px;">' + data.time + '</span>' + '</p>';

                msg.innerHTML = msg.innerHTML + text;
            }
        }
        else {
            var message = data.message;
            msg.innerHTML = ''; 
        }
        msg.scrollTop = msg.scrollHeight;
    });
    
    var ntf_btn = document.getElementById('ntf_btn');
    
    if (window.Notification) {
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
            alert("This browser does not support desktop notification");
        }

        // Let's check whether notification permissions have already been granted
        else if (Notification.permission === "granted") {
            // If it's okay let's create a notification
            var notification = new Notification("Hi there!");
        }

        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== "denied") {
            Notification.requestPermission().then(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification("Hi there!");
            }
            });
        }
    }
}


window.onload = function() {
    init();
}